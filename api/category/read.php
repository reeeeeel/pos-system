<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/database.php';
include_once '../object/category.php';

$database = new database();
$db = $database->get_connection();

$category = new category($db);

$stmt = $category->read();
$num = $stmt->rowCount();

if($num > 0){
    $category_arr = array();
    $category_arr["record"] = array();

    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);

        $category_item = array(
            "cate_name"=>$cate_name
        );
        array_push($category_arr["record"], $category_item);
    }
    http_response_code(200);
    print(json_encode($category_arr));
}else{
    http_response_code(404);
    print(json_encode(array("message"=>"No category was found")));
}