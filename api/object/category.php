<?php
class category
{
    public $conn = null;
    private $table_conn = "category";

    public $name;

    public function __construct($db)
    {
        $this->conn = $db;
    }
    function read(){
        $query = "SELECT cate_name FROM " . $this->table_conn;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

}