<?php

class database
{
    private $host = "127.0.0.1";
    private $username = "root";
    private $password = "20001";
    private $db_name = "pos";
    public $conn;

    public function get_connection(){
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch (Exception $exc){
            print ("Cannot Connect to database". $exc->getMessage());
        }
        return $this->conn;
    }
}