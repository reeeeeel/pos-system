create database pos;
create table category(
  id int(11) not null primary key  auto_increment,
  cate_name varchar(30)
);
create table user(
    id int(11) not null primary key auto_increment ,
    username varchar(30) not null ,
    password varchar(30) not null ,
    account_type int(1) not null
);
create table product(
    id int(11) not null primary key auto_increment,
    p_name varchar(35) not null ,
    qty int not null ,
    price double not null ,
    discount double not null ,
    tax double not null ,
    c_id int(11) not null,
    constraint c_fk foreign key (c_id) references category(id),
    user_id int(11) not null ,
    constraint user_fk foreign key (user_id) references user(id)
);
create table customer(
    id int(11) not null primary key auto_increment ,
    name varchar(30) not null ,
    contact varchar(15)
);
create table invoice(
    id int(11) not null primary key auto_increment,
     cus_id int(11) not null ,
     total_amount int not null ,
     total_price double not null,
     user_id int(1) not null ,
     date date not null ,
     constraint cus_fk foreign key (cus_id) references customer(id),
     constraint user_ifk foreign key (user_id) references user(id)

);
create table sale(
    id int(11) not null primary key auto_increment,
    inv_id int(11) not null ,
    pro_id int(11) not null ,
    qty int not null ,
    unit_price double not null ,
    sub_total double not null ,
    constraint inv_fk foreign key (inv_id) references invoice(id),
    constraint pro_fk foreign key (pro_id) references product(id)
) 